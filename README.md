# docker-nimble

## Getting started

Install docker and docker compose

Modify env file to where you have your nimble files
Run `docker-compose up` to build the container
Run `docker container list` to see the name of the nimble container that is running
Run `docker exec -it <name> bash` to get bash shell in container

This is minimal and only meant to use the bash shell to run things edited in the host OS


